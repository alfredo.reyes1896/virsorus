import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { alertOptions } from 'src/app/core/models/alertOptions.model';
import { Categoria } from 'src/app/core/models/categoria.model';
import { CategoriaDTO } from 'src/app/core/models/categoriaDto.model';
import { AlertsService } from 'src/app/shared/services/alert.service';
import { CategoriasService } from 'src/app/shared/services/categorias.service';

@Component({
  selector: 'app-categorias-page',
  templateUrl: './categorias-page.component.html',
  styleUrls: ['./categorias-page.component.css']
})
export class CategoriasPageComponent implements OnInit {

  categoriaForm: FormGroup;
  categorias: CategoriaDTO[] = [];
  mdlSampleIsOpen: boolean = false;
  esActualizacion: boolean = false;
  options: alertOptions = new alertOptions();
  id: number=0;



  constructor(
    private categoriasService: CategoriasService,
    private formBuilder: FormBuilder,
    private alertService: AlertsService
  ) {

    this.categoriaForm = this.formBuilder.group({
      clave: ['', Validators.required],
      nombre: ['', Validators.required]
    });
  }


  ngOnInit(): void {
    this.getCategorias();
  }

  showGuardar(open: boolean): void {
    this.inicializarForm();
    this.esActualizacion = false;
    this.mdlSampleIsOpen = true;

  }

  onSubmit() {
    (!this.esActualizacion) ? this.guardar() : this.actualizar();
  }


  getCategorias() {
    this.categoriasService.obtenerTodas().subscribe(
      (res: any) => {
        this.categorias = res.data;
      }
    );
  }

  eliminar(id: number) {

    this.options.title = "Eliminar categoria";
    this.options.body = "Se eliminará la categoria ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {
        if (result.isConfirmed) {
          this.alertService.showLoading();
          this.categoriasService.eliminar(id).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se eliminó la categoria";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `La categoria se eliminó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.getCategorias();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo eliminar la categoria";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `La categoria no se eliminó correctamente`;
              this.alertService.show(this.options);

            }
          );


        }
      });

  }

  showActualizar(categoria: any) {
    this.inicializarForm();
    this.esActualizacion = true;
    this.mdlSampleIsOpen = true;
    this.id= categoria.id;
    this.categoriaForm = this.formBuilder.group({
      clave: [categoria.clave, Validators.required],
      nombre: [categoria.nombre, Validators.required]
    });


  }

  guardar() {

    Object.values(this.categoriaForm.controls).forEach(control => {
      control.markAsTouched();
    });

    
    if (this.categoriaForm.invalid) {
      return;
    }

    let categoria: Categoria = this.categoriaForm.value as Categoria;
    this.options.title = "Guardar la categoria";
    this.options.body = "Se guardará la categoria ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {
        if (result.isConfirmed) {

          this.alertService.showLoading();
          categoria.fechaCreado = Date.now();
          this.categoriasService.guardar(categoria).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se guardó la categoria";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `La categoria se guardó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.getCategorias();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo guardar la categoria";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `La categoria no se guardó correctamente`;
              this.alertService.show(this.options);

            }
          );
        }
      });


  }

  actualizar() {
    let categoria: Categoria = this.categoriaForm.value as Categoria;
    categoria.fechaCreado = Date.now();

    this.options.title = "Actualizar categoria";
    this.options.body = "Se actualizará la categoria ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {

        if (result.isConfirmed) {
          this.alertService.showLoading();
          this.categoriasService.actualizar(this.id,categoria).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se actualizó la categoria";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `La categoria se actualizó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.getCategorias();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo actualizar la categoria";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `La categoria no se a actualizó correctamente`;
              this.alertService.show(this.options);

            }
          );
        }
      });

  }

  inicializarForm() {
    this.categoriaForm = this.formBuilder.group({
      clave: ['', Validators.required],
      nombre: ['', Validators.required]
    });
  }


}
