import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticulosRoutingModule } from './articulos-routing.module';
import { ArticulosPageComponent } from './pages/articulos-page/articulos-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ArticulosPageComponent
  ],
  imports: [
    CommonModule,
    ArticulosRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ArticulosModule { }
