import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { alertOptions } from 'src/app/core/models/alertOptions.model';
import { Articulo } from 'src/app/core/models/articulo.model';
import { ArticuloDTO } from 'src/app/core/models/articuloDto.model';
import { CategoriaDTO } from 'src/app/core/models/categoriaDto.model';
import { AlertsService } from 'src/app/shared/services/alert.service';
import { ArticulosService } from 'src/app/shared/services/articulos.service';
import { CategoriasService } from 'src/app/shared/services/categorias.service';

@Component({
  selector: 'app-articulos-page',
  templateUrl: './articulos-page.component.html',
  styleUrls: ['./articulos-page.component.css']
})
export class ArticulosPageComponent implements OnInit {

  articuloForm: FormGroup;
  mdlSampleIsOpen: boolean = false;
  esActualizacion: boolean = false;
  categorias: CategoriaDTO[] = [];
  articulos: ArticuloDTO[] = [];
  articulosCategoria: ArticuloDTO[] = [];
  claveCategoria: string = "";
  options: alertOptions = new alertOptions();
  id: number =0;
  articuloOr: any;

  constructor(private formBuilder: FormBuilder,
    private articulosService: ArticulosService,
    private categoriasService: CategoriasService,
    private alertService: AlertsService) {
    this.articuloForm = this.formBuilder.group({
      clave: ['', Validators.required],
      categoria: ['', Validators.required],
      nombre: ['', Validators.required],
      precios: this.formBuilder.array([
        this.formBuilder.control('', Validators.required) // Al menos un precio requerido
      ]),
      activo: [false, Validators.nullValidator]
    });

    this.getCategorias();
  }


  get preciosArray() {
    return this.articuloForm.get('precios') as FormArray;
  }

  agregarPrecio() {
    this.preciosArray.push(this.formBuilder.control('', Validators.required));
  }

  quitarPrecio(index: number) {
    this.preciosArray.removeAt(index);
  }

  ngOnInit(): void {
  }


  onSubmit() {
    (!this.esActualizacion) ? this.guardar() : this.actualizar();

  }

  showGuardar(open: boolean): void {
    this.inicializarForm();
    this.esActualizacion = false;
    this.mdlSampleIsOpen = true;

  }

  guardar() {

   
    Object.values(this.articuloForm.controls).forEach(control => {
      control.markAsTouched();
    });
    
   

    if (this.articuloForm.invalid) {
      return;
    }


    let precios = this.articuloForm.value.precios.map((precio: number) => ({ precio }));
    
    let articulo = {
      ...this.articuloForm.value,
      precios
    };

    this.options.title = "Guardar articulo";
    this.options.body = "Se guardará el articulo ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {
        if (result.isConfirmed) {

          this.alertService.showLoading();

          this.articulosService.guardar(articulo).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se guardó el articulo";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `El articulo se guardó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.obtenerTodos();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo guardar el articulo";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `El articulo no se guardó correctamente`;
              this.alertService.show(this.options);

            }
          );


        }
      }
    );

  }


  getCategorias() {
    this.categoriasService.obtenerTodas().subscribe(
      (res: any) => {
        this.categorias = res.data;
        this.claveCategoria = this.categorias[0].clave;
        this.obtenerTodos();
      }
    );
  }

  obtenerTodos() {
    this.articulosService.obtenerTodos().subscribe(
      (res: any) => {
        this.articulos = res.data;
        this.articulosCategoria = this.articulos.filter(a => a.categoria.clave == this.claveCategoria);
      }
    );
  }

  gatArticulosCategoria() {
    this.articulosCategoria = this.articulos.filter(a => a.categoria.clave == this.claveCategoria);

  }

  eliminar(id: number) {

    this.options.title = "Eliminar articulo";
    this.options.body = "Se eliminará el articulo ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {
        if (result.isConfirmed) {
          this.alertService.showLoading();
          this.articulosService.eliminar(id).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se eliminó el articulo";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `El articulo se eliminó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.obtenerTodos();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo eliminar el articulo";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `El articulo no se eliminó correctamente`;
              this.alertService.show(this.options);

            }
          );
        }
      });

  }

  showActualizar(articulo: ArticuloDTO) {
    this.inicializarForm();
    this.esActualizacion = true;
    this.mdlSampleIsOpen = true;
    this.id= articulo.id;
    this.articuloForm = this.formBuilder.group({
      clave: [articulo.clave, Validators.required],
      categoria: [articulo.categoria.id, Validators.required],
      nombre: [articulo.nombre, Validators.required],
      precios: this.formBuilder.array([
        this.formBuilder.control(articulo.precios[0].precio, Validators.required) 
      ]),
      activo: [false, Validators.nullValidator]
    });
    this.articuloOr =  articulo;

    articulo.precios.slice(1).forEach(p => {
      this.preciosArray.push(this.formBuilder.control(p.precio, Validators.required));
    });


  }

  actualizar() {

    let precios = this.articuloForm.value.precios.map((precio: number) => ({ precio }));
  
   // precios= this.articuloOr.precios;

   this.articuloOr.precios.forEach(
     (p: any) =>{
         let i  = precios.findIndex((pp:any) => pp.precio == p.precio );
         console.log(p,i);
         precios[i]= p;
     }
   );

    console.log(precios);
    let articulo = {
      ...this.articuloForm.value,
         precios
    };
   

    articulo.categoria = Number.parseInt(articulo.categoria);

    this.options.title = "Actualizar articulo";
    this.options.body = "Se actualizará el articulo ¿Desea continuar?";
    this.options.type = "warning";
    this.options.showCancelButton = true;
    this.options.showCancelButton = true;


    this.alertService.show(this.options).then(
      result => {

        if (result.isConfirmed) {
          this.alertService.showLoading();
          this.articulosService.actualizar(this.id, articulo).subscribe(
            (res: any) => {
              this.alertService.closeLoading();
              this.options.title = "Se actualizó el articulo";
              this.options.type = "success";
              this.options.showCancelButton = false;
              this.options.body = `El articulo se actualizó correctamente`;
              this.alertService.show(this.options);
              this.mdlSampleIsOpen = false;
              this.obtenerTodos();

            }, (error: any) => {
              this.alertService.closeLoading();
              this.options.title = "No se pudo actualizar el articulo";
              this.options.type = "error";
              this.options.showCancelButton = false;
              this.options.body = `El articulo no se actualizó correctamente`;
              this.alertService.show(this.options);

            }
          );

        }
      });

  }

  inicializarForm() {
    this.articuloForm = this.formBuilder.group({
      clave: ['', Validators.required],
      categoria: ['', Validators.required],
      nombre: ['', Validators.required],
      precios: this.formBuilder.array([
        this.formBuilder.control('', Validators.required) 
      ]),
      activo: [false, Validators.nullValidator]
    });
  }



}
