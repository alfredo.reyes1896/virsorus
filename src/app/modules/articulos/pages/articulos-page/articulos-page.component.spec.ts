import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulosPageComponent } from './articulos-page.component';

describe('ArticulosPageComponent', () => {
  let component: ArticulosPageComponent;
  let fixture: ComponentFixture<ArticulosPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ArticulosPageComponent]
    });
    fixture = TestBed.createComponent(ArticulosPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
