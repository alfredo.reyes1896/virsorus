import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticulosModule } from '../articulos/articulos.module';
import { ArticulosPageComponent } from '../articulos/pages/articulos-page/articulos-page.component';
import { CategoriasModule } from '../categorias/categorias.module';
import { CategoriasPageComponent } from '../categorias/pages/categorias-page/categorias-page.component';

const routes: Routes = [
  {
    path: 'categorias',
    component: CategoriasPageComponent,
    loadChildren:() => {return CategoriasModule;} 
  },
  {
    path: 'articulos',
    component: ArticulosPageComponent,
    loadChildren:() => {return ArticulosModule;} 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
