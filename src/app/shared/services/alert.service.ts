import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { alertas, alertOptions } from 'src/app/core/models/alertOptions.model';
import Swal, { SweetAlertOptions } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})


export class AlertsService {


  constructor() { }

  show(data: alertOptions) {
    let iconTitle = "";
    switch (data.type) {
      case "error":
        iconTitle = `<img src="assets/icons/error.svg"`;
        break;
      case "warning":
        iconTitle = `<img src="assets/icons/warning.svg"`;
        break;

      case "success":
        iconTitle = `<img src="assets/icons/success.svg"`;
        break;

    }


    return alertas.fire(
      {
        allowEscapeKey: false,
        showConfirmButton: data.showConfirmButton,
        showCancelButton: data.showCancelButton,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true,
        confirmButtonColor: '#3085d6',
        allowOutsideClick: false,
        html: `
      <div style="font-family: 'Source Sans Pro', sans-serif;">
        <div style=" display: flex; align-items: center;">
          ${iconTitle}
          <span style="margin-left: 5px; fontt-size: 30px; color: black">${data.title}</span>
        </div>
        <hr>
        <p style="font-size: 14px; text-align: left; padding-top: 20px">
        ${data.body}
         </p>
      <div>
      `,

      }
    ).then((result) => {
      return result;
    })
  }

  showLoading(){
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
  }

  closeLoading(){
     Swal.close();
  }
}
