import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environments';
import { Categoria } from 'src/app/core/models/categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {


  constructor(
    private http: HttpClient
  ) { }


  guardar(categoria: Categoria){
    return this.http.post(`${environment.URI}/categoria`,categoria);
  }

  obtenerTodas(){
    return this.http.get(`${environment.URI}/categoria`);
  }

  eliminar(id: number){
    return this.http.delete(`${environment.URI}/categoria/${id}`);

  }

  obtener(id: number){
    return this.http.get(`${environment.URI}/categoria/${id}`);

  }
  actualizar(id: number, categoria: Categoria){
    return this.http.put(`${environment.URI}/categoria/${id}`, categoria);

  }
}
