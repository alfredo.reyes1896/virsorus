import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Articulo } from 'src/app/core/models/articulo.model';
import { environment } from 'src/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {

  constructor(
    private http: HttpClient
  ) { }

  guardar(articulo: Articulo){
    return this.http.post(`${environment.URI}/articulo`,articulo);
  }

  obtenerTodos(){
    return this.http.get(`${environment.URI}/articulo`);

  }

  eliminar(id: number){
    return this.http.delete(`${environment.URI}/articulo/${id}`);

  }

  actualizar(id: number, articulo: Articulo){
    return this.http.put(`${environment.URI}/articulo/${id}`, articulo);

  }

}
