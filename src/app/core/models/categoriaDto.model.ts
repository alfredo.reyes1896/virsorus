export interface CategoriaDTO{
    id: number;
    nombre: string;
    version: number;
    fechaCreado: number;
    clave:string;
    categoria: any;
    activo: boolean;
    categorias: any[];
}