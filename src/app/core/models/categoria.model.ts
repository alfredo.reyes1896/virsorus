export interface Categoria{
     clave: string;
     fechaCreado: number;
     nombre: string;
}