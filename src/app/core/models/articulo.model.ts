export interface Articulo{
    clave:string;
    categoria: number;
    nombre: string;
    precios: { precio: number }[];
    activo:boolean
}