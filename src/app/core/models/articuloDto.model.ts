import { CategoriaDTO } from "./categoriaDto.model";

export interface ArticuloDTO {
    id: number;
    nombre: string;
    activo: boolean;
    categoria: CategoriaDTO;
    clave: string;
    precios: { precio: number }[];
    version: number;
}