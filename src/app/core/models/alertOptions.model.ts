import Swal, { SweetAlertOptions } from "sweetalert2";

export class alertOptions {
    title: string = ""; //titulo
    body: string = ""; //contenido 
    type: string = "" // tipo : warning , error , success
    showCancelButton: boolean = false;
    showConfirmButton: boolean = true;
    
}

export const alertas= Swal.mixin({
    customClass: {
      title: 'title-sweetalert',
      icon: 'icon-sweetalert',
    },
  });
  